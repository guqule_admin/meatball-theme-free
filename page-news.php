<!doctype html>
<html lang="zh">
<head>
    <?php
    // TEMPLATE NAME: 快讯页面
    get_header(); ?>
</head>
<body>
<?php get_template_part('component/body-top'); ?>
<div id="app">
    <header>
        <div class="header-main-plane">
            <div class="header-main container">
                <?php
                get_template_part('component/nav-header');
                ?>
            </div>
        </div>
    </header>
    <div class="top-divider"></div>
    <main class="container">
        <div class="html-main">
            <?php
            global $set;
            if ($set['theme']['sidebar']['other']) {
                ?>
                <style>
                    body .post-info-right {
                        display: none;
                    }

                    .post-item-thumbnail img {
                        max-width: 336px;
                    }
                    .post-item-content,.post-item-info  {
                        font-size: 16px;
                    }
                    .post-item h2 {
                        font-size: 22px;

                    }
                </style>
                <div class="post-main post-main-closesidebar" style="flex-basis: 100%;">
                    <?php
                    get_template_part('component/post-news');
                    ?>
                </div>
                <?php
            } else
                if ($set['theme']['sidebar_position'] == 1) {
                    ?>
                    <div class="post-main">
                        <?php get_template_part('component/post-news'); ?>
                    </div>
                    <div class="sidebar">
                        <?php dynamic_sidebar('index_sidebar'); ?>
                    </div>
                    <?php
                } else {
                    ?>
                    <div class="sidebar">
                        <?php dynamic_sidebar('index_sidebar'); ?>
                    </div>
                    <div class="post-main">
                        <?php get_template_part('component/post-news'); ?>
                    </div>
                    <?php
                }
            ?>
        </div>
    </main>
    <footer>
        <?php
        wp_footer();
        get_footer(); ?>
    </footer>
</div>
<script>

    jQuery(document).ready(function ($) {

        $('.share-icon').click(function (){
            var e = $(this),
                i = e.closest(".kx-item"),
                n = "";
            console.log(i);
            var o = encodeURIComponent($.trim(i.find("h2").text())),
                a = encodeURIComponent($.trim(i.find(".kx-content p").text()).replace("[原文链接]", "")),
                s = e.closest(".kx-meta").data("url"),
                r = "";
            i.find(".kx-content img").length && (r = encodeURIComponent(i.find(".kx-content img").attr("src")))

            if (e.hasClass("weibo")) n = "http://service.weibo.com/share/share.php?url=" + s + "&title=" + o + "&pic=" + r + "&searchPic=true";
            else if (e.hasClass("qq")) n = "https://connect.qq.com/widget/shareqq/index.html?url=" + s + "&title=" + o + "&desc=" + a + "&summary=&site=&pics=" + r;
            else if (e.hasClass("copy")) if (void 0 !== document.execCommand) {
                var l = decodeURIComponent(o) + "\r\n" + decodeURIComponent(a) + "\r\n" + decodeURIComponent(s),
                    d = document.createElement("textarea");
                d.value = l,
                    $("body").append(d),
                    d.style.position = "fixed",
                    d.style.height = 0,
                    d.select(),
                    document.execCommand("copy"),
                    d.remove(),
                    alert("复制成功！")
            } else alert("浏览器暂不支持拷贝功能");
            n && window.open(n)
        })




    })
</script>
</body>
</html>
<?php
