<?php
$paged = get_query_var('paged') ? get_query_var('paged') : 1;
$args = array(
    'post_status' => array('publish'),
    'post_type' => 'post',
    'paged' => $paged,
);
$cur_day = '';
$weekarray = array("日", "一", "二", "三", "四", "五", "六");
query_posts($args);
// 主循环
if (have_posts()) : while (have_posts()) : the_post();
    ?>

    <div class="kx-list">
        <?php
        if ($cur_day != $date = get_the_date(get_option('date_format'))) {
            $pre_day = '';
            $week = $weekarray[date('w', strtotime(get_the_date('c')))];
            if (date(get_option('date_format'), time()) == $date) {
                $pre_day = '今天 • ';
            } else if (date(get_option('date_format'), strtotime("-1 day")) == $date) {
                $pre_day = '昨天 • ';
            } else if (date(get_option('date_format'), strtotime("-2 day")) == $date) {
                $pre_day = '前天 • ';
            }
            echo '<div class="kx-date">' . $pre_day . $date . ' • 星期' . $week . '</div>';
            if ($cur_day == '') echo '<div class="kx-new"></div>';
            $cur_day = $date;
        } ?>
        <div class="kx-item" data-id="<?php the_ID(); ?>">
            <span class="kx-time"><?php the_time(get_option('time_format')); ?></span>
            <div class="kx-content">
                <h2><a href="<?php the_permalink(); ?>"
                       target="_blank"><?php the_title(); ?></a></h2>
                <?php the_excerpt(); ?>
                <?php $imagelist = get_post_images($post->ID); ?>
                <a class="kx-img" href="<?php the_permalink(); ?>"
                   title="<?php echo esc_attr(get_the_title()); ?>"
                   target="_blank"><img src="<?php echo $imagelist[0]; ?>"
                                        class="rounded posts-top-image" alt=""></a>
            </div>

            <div class="kx-meta hidden-xs clearfix"
                 data-url="<?php echo urlencode(get_permalink()); ?>">
                <span>分享到</span>
                <span class="share-icon weibo">微博</span>
                <span class="share-icon qq">QQ</span>
                <span class="share-icon copy">链接</span>
            </div>

        </div>
    </div>

<?php endwhile; ?>
<?php endif; ?>
<div class="pages">
    <?php
    if ($set['theme']['paging'] == 'ajax') {
        get_template_part('component/pageobj-ajax');

    } else {
        get_template_part('component/pageobj');

    }
    ?>
</div>

<?php wp_reset_query(); ?>

