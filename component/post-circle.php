<?php
global $current_user;
$paged = get_query_var('paged') ? get_query_var('paged') : 1;
$num = 20;
$args = array(
    'taxonomy' => 'circle',
    'parent' => 0,
    'number' => $num,
    'hide_empty' => false,
    'offset' => $num * ($paged - 1)
);
$circle = get_terms($args);

?>


<div class="forum-box">

    <?php foreach ($circle as $sp) { ?>
        <div class="forum">
            <div class="blocks">
                <div class="title"> <?php echo $sp->name ?></div>
                <div class="form-blocks">
                    <?php $list = get_terms(array('taxonomy' => 'circle', 'hide_empty' => false, 'parent' => $sp->term_id)) ?>
                    <?php foreach ($list as $lp) {  ?>

                        <div class="blocks-boxs">
                            <div class="lefts">
                                <div class="blocks-images">
                                    <img src="<?php echo get_term_meta($lp->term_id, 'cover', true); ?>" alt="版块logo"
                                         class="blocks-img">
                                </div>
                                <div class="boxs-text">
                                    <div class="block-titles"><?php echo $lp->name ?> </div>
                                     <div class="block-nums">
                                         <div><?php $init = new MP_Circle(); echo  $init->counts( array( "term_id" => $lp->term_id) )?> <span>关注</span></div>
                                         <div> <?php echo $lp->count;?><span>帖子</span></div>
                                     </div>
                                </div>
                            </div>
                            <div class="forum-follow">
                                <?php $static=get_itaxonomies_follow_status($current_user->id,$lp->term_id);?>
                                <button><?php if($static){echo '已关注';}else{echo '+关注';}?></button>
                            </div>
                        </div>
                    <?php } ?>
                </div>
            </div>


        </div>
    <?php } ?>


</div>