<?php
global $set;

$postitem['post_meta'] = json_decode(get_post_meta($post->ID, 'corepress_post_meta', true), true);


if (has_excerpt()) {
    $postitem['content'] = get_the_excerpt();
    if (strlen(preg_replace("/[\s]{2,}/", "", $postitem['content'])) == 0) {
        $postitem['content'] = mb_strimwidth(strip_tags(apply_filters('the_content', $post->post_content)), 0, $set['routine']['summary_lenth'], "……");
    }
} else {
    $postitem['content'] = mb_strimwidth(strip_tags(apply_filters('the_content', $post->post_content)), 0, $set['routine']['summary_lenth'], "……");
}

if (post_password_required()) {
    $postitem['content'] = '内容已加密，请输入密码以后查看';
}
$postitem['thumbnail'] = null;

if (has_post_thumbnail()) {
    if (!isset($postitem['post_meta']['postshow'])) {
        $postitem['post_meta']['postshow'] = null;
    }
    if ($postitem['post_meta']['postshow'] == 1) {
        $postitem['thumbnail'] = get_the_post_thumbnail_url($post, 'full');
    } else {
        $postitem['thumbnail'] = get_the_post_thumbnail_url($post, 'full');
    }
} else if ($postitem['post_meta']['thumbnail'] != '') {
    $postitem['thumbnail'] = $postitem['post_meta']['thumbnail'];
} else if ($set['routine']['autothumbnail'] == 1) {
    $preg = '/<img.*?src=[\"|\']?(.*?)[\"|\']?\s.*?>/i';
    preg_match($preg, $post->post_content, $imgArr);
    if (count($imgArr) != 0) {
        $postitem['thumbnail'] = $imgArr[1];
    }
}
$circle = wp_get_post_terms($post->ID, 'circle');

preg_match_all('/<img.*?(?: |\\t|\\r|\\n)?src=[\'"]?(.+?)[\'"]?(?:(?: |\\t|\\r|\\n)+.*?)?>/sim', $post->post_content, $result, PREG_PATTERN_ORDER);
$imageslist = $result[1];

if ($postitem['thumbnail'] == null) {
    $postitem['thumbnail'] = $set['routine']['defaultthumbnail'];
}

$postitem['views'] = null;
if (function_exists('the_views')) {
    $postitem['views'] = intval(get_post_meta($post->ID, 'views', true));
}
$postitem['url'] = get_the_permalink();
$postitem['author'] = get_the_author();
$postitem['author_avatar'] = get_avatar(get_the_author_meta('email'), 24, '', '', array('class' => 'post-item-avatar'));


$postitem['commentsnum'] = get_comments_number();
$postitem['title'] = get_the_title();
$postitem['category'] = get_the_category();
$postitem['time'] = get_the_time('Y-m-d');
$newpost = '';
if ($set['theme']['postlist_newnote'] == 1) {
    if (date("Y-m-d") == $postitem['time']) {
        $newpost = 'post-item-new';
    }
}

foreach ($postitem['category'] as $item) {
    $item->url = get_category_link($item->cat_ID);
}
$target = '';
if ($set['routine']['opennewlink'] == 1) {
    $target = '_blank';
}
if ($set['module']['imglazyload'] == 1) {
    $pathname = 'data-original';
    $imgtag = '<img src="' . file_get_img_url('load.png') . '" data-original="' . $postitem['thumbnail'] . '">';
} else {
    $imgtag = '<img src="' . $postitem['thumbnail'] . '">';
}
if (empty($postitem['post_meta']['postshow'])) {
    $postitem['post_meta']['postshow'] = 0;
}
if ($postitem['post_meta']['postshow'] == 1) {
    ?>
    <li class="post-item post-item-type1 <?php echo $newpost ?>">
        <h2>
            <?php
            if (is_sticky(get_the_ID())) {
                ?>
                <span class="post-item-sticky">置顶</span>
                <?php
            };
            ?><a href="<?php echo $postitem['url'] ?>"
                 target="<?php echo $target; ?>"><?php echo $postitem['title']; ?></a>
        </h2>
        <div class="post-item-thumbnail-type1">
            <a href="<?php echo $postitem['url'] ?>" target="<?php echo $target; ?>"><?php echo $imgtag ?></a>
        </div>
        <div class="post-item-tags post-item-tags-type1">
            <?php
            foreach ($postitem['category'] as $catite) {
                ?>
                <span class="cat-item"><a
                            target="<?php echo $target ?>"
                            href="<?php echo $catite->url ?>"><?php echo $catite->name ?></a></span>
                <?php
                break;
            }
            ?>
        </div>

        <div class="post-item-content post-item-content-type1">
            <?php echo $postitem['content'] ?>
        </div>
        <div class="post-item-info post-item-info-type1">

            <div class="post-item-meta">
                <div class="post-item-meta-time">
                 <span class="post-item-meta-author">
                               <?php echo $postitem['author_avatar'] .
                                   $postitem['author'] ?>
                            </span>
                    <span class="post-item-time"><?php echo diffBetweenTwoDay($postitem['time']); ?></span>
                </div>
                <div class="item-post-meta-other">
                    <?php
                    if ($postitem['views'] !== null) {
                        echo '<span><i class="fas fa-eye"
                                 aria-hidden="true"></i>';
                        echo views_convert($postitem['views']) . '</span>';
                    }
                    ?>
                    <span><i class="fas fa-comment-alt-lines"></i><?php echo views_convert($postitem['commentsnum']) ?></span>
                </div>
            </div>
        </div>
    </li>
    <?php
} else {
    ?>
    <li class="recommend-list <?php echo $newpost ?>">
        <div class="news-post">
            <div class="list-msg">
                <div class="certification">
                    <img src="<?php echo the_author_meta('avatar'); ?>" alt="用户头像" class="video-avatar">
                </div>
                <div class="user-time">
                    <?php $grade = miniprogram_credit_grade_title(get_the_author_id()); ?>
                    <span class="video-user"><p><?php the_author(); ?> </p>
                        <div class="grade">
                       <img src="<?php echo $grade['icon'] ?>"
                            alt="" class="user-grade"> <span
                                    style="color:#999;font-size: 10px;"> <?php echo $grade['title'] ?></span>
                    </div>
                    </span>
                    <p class="list-time">
                        <?php echo $postitem['time'] ?>
                        <span class="list-actor"> 个性签名:<?php echo the_author_meta('description'); ?></span></p>
                </div>
            </div>
            <div class="post-images">
                <div class="post-contents">
                    <a href="<?php echo $postitem['url'] ?>">
                        <?php the_excerpt(); ?>
                    </a>
                </div>

                <div class="post-images position-images" style="display: flex; flex-wrap: wrap;">
                    <?php foreach ($imageslist as $key => $value) : ?>
                        <?php if ($key == 9) {
                            break;
                        } ?>
                        <div class="list-img">
                            <?php echo '<img class="pic" src="' . file_get_img_url('load.png') . '" data-original="' . $value . '">' ?>
                        </div>
                    <?php endforeach; ?>

                </div>

            </div>
            <div class="more-msg">
                <div>
                    <div class="card-circle"><img
                                src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABwAAAAaCAYAAACkVDyJAAACC0lEQVRIibWVv2sUURSFvzs7WbIxqCioEZuAAQuFgIgQQbSzSqG9aWJs0ljYWNpoJUIadwIi2In+A0JQsLBTCy2MrmIRFTWEqOAm7M6R/T07O5uZ58ZTzZtv3jnzLve9R1Z5RV3xAq3lAi0kTUnjLVmWvMKiDm2IEiKPUT09wfDTs1bJyrt+LEvgJpyqmzX0MW6Wxp0DLWSy/QyvXLlzoOgYSr2GaXygwKQVpHGnwB2L2g8caI3zw92GaTyuepeeeSL/2TK3gZNYp3Ol+vMIcCQy70XMI5Gb1RbeMqIi40E4Z7fq5rlAFyQepq12UOXzjNdL6kEJCP9z3upoyGq7fEOBJkM4ES0pjbLeQOyhUaY78ZIKbnZx42VPlKgMeSyVZ+3TlifNwaJGvsKvVnP5xtHNOXuTlSdpyy79Dsfa3xjlqQneunDnwGp0f4nX8SMrjTsHmnUMSTrSUniiZy7QNOIqRiEOJQ4Du5rDL2Z8duENEz4UCsz/vmjfakNf4i6wN7JN+2lMYuwf+PFymffAtdrAM+NdllIMIsFya7q/U5z7aUz3lDRkSjDTHK2Yx3Un3pQZpcqsLbUD1y7bOnA//qFf1Lg6k55XL1ngwvupb5du5x0YVabAQe/A1MDtvgNTAzfUtaF//JmxFRfuHKjIe8+458q3kp8Ez+/m8aN15hGj+8RC/PhI430F/AUaW+31CQiFzgAAAABJRU5ErkJggg=="
                                alt=""><span><a
                                    target="<?php echo $target ?>"
                                    href="<?php echo get_term_link($circle[0]->term_id, 'circle') ?>"><?php echo $circle[0]->name; ?></a></span>
                    </div>
                </div>
                <div class="view-comment">
                 <span class="is-support">
                     <i class="osxiconfont iconpraise_icon" style="font-size: 16px;"></i>
                     <?php echo mp_comment_count($post->ID, 'like') ?></span>
                    <span class="is-support">
                     <i class="osxiconfont iconconversation_icon" style="font-size: 16px;"></i>
                     <?php echo mp_comment_count($post->ID, 'comment') ?></span>
                    <span class="is-support">
                     <i class="osxiconfont iconattention" style="font-size: 16px;"></i>
                    <?php echo (int)get_post_meta($post->ID, "views", true) ?></span>
                    <span class="is-support">
                     <i class="osxiconfont iconcollect_icon" style="font-size: 16px;"></i>
                    <?php echo mp_comment_count($post->ID, 'fav') ?></span>
                </div>
            </div>

        </div>
    </li>
    <?php
}
?>


