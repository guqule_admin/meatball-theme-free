<?php


class CorePress_credits_Rank_widget extends WP_Widget
{
    function __construct()
    {
        parent::__construct(
            'corepress_credits_widget',
            'CorePress积分排行',
            array(
                'description' => '显示积分排名'
            )
        );
    }


    function form($instance)
    {
        $num = isset($instance['number']) ? absint($instance['number']) : 10;
        $title = isset($instance['title']) ? $instance['title'] : '积分排名';
        $type = isset($instance['type']) ? $instance['type'] : 'comment';

        ?>
        <p><label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:'); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>"
                   name="<?php echo $this->get_field_name('title'); ?>" type="text"
                   value="<?php echo esc_attr($title); ?>"/></p>

        <p><label for="<?php echo $this->get_field_id('number'); ?>">显示数量</label>
            <input class="tiny-text" id="<?php echo $this->get_field_id('number'); ?>"
                   name="<?php echo $this->get_field_name('number'); ?>" type="number" step="1" min="1"
                   value="<?php echo $num; ?>" size="3"/></p>
        <?php
    }

    public function widget_start($args, $instance)
    {
        echo $args['before_widget'];
        if ($title = apply_filters('widget_title', empty($instance['title']) ? '' : $instance['title'], $instance, $this->id_base)) {
            echo $args['before_title'] . $title . $args['after_title'];
        }
    }

    public function widget_end($args)
    {
        echo $args['after_widget'];
    }

    function update($new_instance, $old_instance)
    {
        /*  $instance = $old_instance;
          $instance['num'] = absint($new_instance['num']);
          $instance['title'] = sanitize_text_field($new_instance['title']);*/
        return $new_instance;
    }

    function widget($args, $instance)
    {

        $this->widget_start($args, $instance);
        $number = (!empty($instance['number'])) ? absint($instance['number']) : 10;
        $args['per_page'] = $number;
        $init = new MP_Credits();
        $credits = $init->credit_query($args);
        $data = array();
        if ($credits) {
            ?>
            <ul class="credit-top-list">
                <?php
                foreach ($credits as $credit) { ?>
                    <li><a href="<?php get_author_link(true, $credit->user_id); ?>" target="_blank"
                           class="link-block"></a>
                        <div class="credit-top-avatar"><img
                                    src="<?php echo get_user_meta($credit->user_id, 'avatar', true); ?>"
                                    class="avatar b2-radius"></div>
                        <div class="credit-top-info">
                            <div class="credit-top-info-left">
                                <div class="credit-top-name">
                                    <span><?php echo get_user_meta($credit->user_id, 'nickname', true) ?></span></div>
                                <p>
                                    <?php $grade = miniprogram_credit_grade_title($credit->user_id); ?>
                                    <span><span style="color:#dd3333"
                                                class="lv-icon user-lv b2-lv5"><b><?php echo $grade['title']; ?></b></span></span><span><span
                                                class="lv-icon user-vip b2-vip0"><img
                                                    src="<?php echo $grade['icon']; ?>"
                                                    style="border-color:#dd3333;width:14px;height:14px;"></img></p>
                            </div>
                            <div class="credit-top-info-right"><span><i
                                            class="b2font b2-coin-line "></i><b><?php echo $credit->credits ?></b></span>
                            </div>
                        </div>
                    </li>
                <?php } ?>
            </ul>
            <?php
        }

        $this->widget_end($args, $instance);
    }


}

// register widget
function register_corepress_credits_widget()
{
    register_widget('CorePress_credits_Rank_widget');
}

add_action('widgets_init', 'register_corepress_credits_widget');