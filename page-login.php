<?php
// TEMPLATE NAME: CorrPress自定义登录页面
if (islogin() && !isset($_GET['bind'])) {
    header("Location: " . get_bloginfo('url'));
    exit();
}


global $set;
require_once(FRAMEWORK_PATH . '/thirdparty/thirdpartylogin.php');


?>
<!doctype html>
<html lang="zh">
<head>
    <?php get_header(); ?>
    <?php
    if (isset($_GET['type']) && ($_GET['type'] == 'wechat' || $_GET['type'] == 'miniprogram')) {
        if (version_compare(get_bloginfo('version'), '5.5', '>=')) {
            echo '<script type="text/javascript" src="//dn-staticfile.qbox.me/jquery/2.1.4/jquery.min.js"></script>';
        } ?>
        <script>
            (function ($) {
                $(function () {
                    $('.login-form').css('display', 'none');
                    $('.login-weapp-form').css('display', 'block');
                    $('.mask').css('display', 'none');
                })
            })(jQuery);
        </script>
    <?php } ?>
</head>
<body>
<?php
file_load_css('login-plane.css');
?>


<div id="app" class="login-background">
    <header>
        <div class="header-main-plane">
            <div class="header-main container">
                <?php
                get_template_part('component/nav-header');
                ?>
            </div>
        </div>
    </header>
    <div class="header-zhanwei" style="min-height: 80px;width: 100%;"></div>

    <style>
        <?php
if ($set['user']['lgoinpageimg'] != null) {
    echo '#app{background-image:url('.$set['user']['lgoinpageimg'].');}';
}
?>
    </style>
    <main class="container">
        <div id="login-plane">
            <div class="login-main">
                <div id="login-note">
                    提示
                </div>
                <div class="login-weapp-form" style="display: none;padding:20px;">
                    <p class="weapp">
                        <label style="font-weight: 600;margin-bottom: 10px;font-size: 14px;line-height: 1.5;display: inline-block;">请使用小程序扫描二维码</label>
                        <label id="qrcode" style="position: relative;text-align: center; display: block;width: 100%;">
                            <label class="mask" style="color:#fff;background-color: rgba(0,0,0,.6);position: absolute;top: 0;left: 0;width: 100%;height: 100%;z-index: 9;display: none;justify-content: center;-webkit-justify-content: center;align-items: center;-webkit-align-items: center;"><span>二维码失效<br>请点击刷新</span></label>
                        </label>
                    </p>
                    <p class="weapp"  style="font-weight: 600;margin-bottom: 10px;font-size: 14px;line-height: 1.5;display: inline-block;"><a href="<?php echo loginAndBack(); ?>">帐号密码登录</a></p>
                </div>
                <div class="login-form">
                    <div class="login-title"><h3>登录账户</h3>
                        <?php if (get_option('users_can_register')) {
                            echo '<span><a href="' . wp_registration_url() . '">注册用户</a></span>';
                        } ?>
                    </div>
                    <i class="fa fa-user ico-login" aria-hidden="true"></i><input class="input-login input-user"
                                                                                  name="user"
                                                                                  type="text"
                                                                                  placeholder="请输入用户名/电子邮箱">
                    <i class="fa fa-key ico-login" aria-hidden="true"></i><input class="input-login input-pass"
                                                                                 name="pass"
                                                                                 type="password"
                                                                                 placeholder="请输入密码">
                    <?php
                    if ($set['user']['VerificationCode'] == 1) {
                        ?>
                        <div class="code-plane"><img class="img-code"
                                                     src="<?php echo FRAMEWORK_URI . "/VerificationCode.php" ?>"
                                                     alt=""><input class="input-login input-code"
                                                                   name="code"
                                                                   type="text"
                                                                   placeholder="验证码"></div>
                        <?php
                    }
                    ?>
                    <div class="login-title">
                        <label><input type="checkbox" id="remember" name="remember" value="true">记住我的登录状态</label>
                        <a href="<?php echo wp_lostpassword_url() ?>">忘记密码?</a>
                    </div>
                    <div class="thirdparty-plane">
                        <?php if ($set['user']['thirdparty_login'] == 1) {
                            if ($set['user']['thirdparty_login_qq']['open'] == 1) {
                                ?>
                                <span class="login-thirdparty-btn"><a
                                            href="<?php echo home_url(add_query_arg(array('thirdparty' => 'qq'))); ?>"><?php file_load_img('icons\QQ.svg'); ?>QQ登录</a></span>
                                <?php
                            }
                        } ?>
                    </div>
                    <p class="weapp weapp-login weapp-login-text"><a
                                href="<?php echo add_query_arg('type', 'miniprogram', wp_login_url()); ?>">小程序扫码登录</a>
                    </p>
                    <div>
                        <button class="login-button" id="btn-login">登录</button>
                    </div>
                </div>
            </div>
        </div>
    </main>
    <script>
        $('.img-code').click(() => {
            recodeimg();
        });
        $('#btn-login').click(() => {
            login();
        })
        $('.input-code,.input-pass').bind('keypress', function (event) {
            console.log(event.keyCode);
            if (event.keyCode == "13") {
                login();
            }
        });

        function login() {
            var user = $('input[name="user"]').val();
            var pass = $('input[name="pass"]').val();
            var code = $('input[name="code"]').val();
            if (user == '' || pass == '') {
                return;
            }
            var remember = $('#remember').val();
            $('#login-note').text('登录中，请稍后');
            $('#login-note').css('visibility', 'visible');
            $.post('<?php echo AJAX_URL?>', {
                action: 'corepress_login',
                user: user,
                pass: pass,
                remember: remember,
                code: code
            }, (data) => {
                var obj = JSON.parse(data);
                if (obj) {
                    if (obj.code === 1) {
                        $('#login-note').text('登录成功，跳转中');
                        window.location.href = getQueryVariable('redirect_to') ? decodeURIComponent(getQueryVariable('redirect_to')) : '/';
                    } else {
                        $('#login-note').text(obj.msg);
                        recodeimg();
                    }
                } else {

                }
            })
        }

        function recodeimg() {
            $('.img-code').attr('src', '<?php echo FRAMEWORK_URI . "/VerificationCode.php?t=" . time() ?>');
        }
    </script>
    <footer>

        <?php
        if (isset($_GET['type']) && $_GET['type'] == 'wechat') {
            wechat_scan_qrcode_action();
        } else if (isset($_GET['type']) && $_GET['type'] == 'miniprogram') {
            miniprogram_scan_qrcode_action();
        } else {

            wp_footer();
            get_footer();
        } ?>
    </footer>
</div>
</body>
</html>

